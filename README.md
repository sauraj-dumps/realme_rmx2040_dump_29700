## full_oppo6769-user 10 QP1A.190711.020 68b77aba7cb33275 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2040
- Brand: realme
- Flavor: full_oppo6769-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 0470_202106102147
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX2040_11.A.47_0470_202106102147
- Branch: RMX2040_11.A.47_0470_202106102147
- Repo: realme_rmx2040_dump_29700


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
